#include "mywidget.h"
#include <QFileDialog>
#include <QMessageBox>
#include <qtextstream.h>

myWidget::myWidget(QWidget *parent)
	: QWidget(parent)
{
	mark=0;
	minx=0;
	miny=0;
	pNum=0;
}

myWidget::~myWidget()
{

}

void myWidget::paintEvent(QPaintEvent *)
{
	int edgNum,xpoi,ypoi;
	QPainter painter(this);
	painter.setRenderHint( QPainter::Antialiasing,true);
	QPen pen; //画笔
	if (mark==1)
	{
		QFile f(fileName);
		if(!f.open(QIODevice::ReadOnly | QIODevice::Text))  
		{  
			QMessageBox::information(this,"警告","打开文件失败"); 
		}
		else
		{
			QTextStream txtInput(&f);
			txtInput>>pNum>>edgNum;
			pen.setColor(QColor(0,255,0));
			pen.setWidth(1);
			painter.setPen(pen);
			while (edgNum>0)
			{
				txtInput>>xpoi>>ypoi;
				painter.drawLine(*(posx+xpoi)-minx,*(posy+xpoi)-miny,*(posx+ypoi)-minx,*(posy+ypoi)-miny);
				edgNum--;
			}
			f.close();
			pen.setColor(QColor(255,0,0));
			pen.setWidth(1);
			QBrush brush(QColor(255,0,0)); //画刷
			painter.setPen(pen);
			painter.setBrush(brush);
			for (int i=0;i<pNum;i++)
			{
				painter.drawEllipse(*(posx+i)-3-minx,*(posy+i)-3-miny,6,6);
			}
			//mark=0;
		}
	}
	else if (mark==2)
	{
		QFile f(fileName);
		if(!f.open(QIODevice::ReadOnly | QIODevice::Text))  
		{  
			QMessageBox::information(this,"警告","打开文件失败"); 
		}
		else
		{
			QTextStream txtInput(&f);
			txtInput>>pNum>>edgNum;
			pen.setColor(QColor(0,255,0));
			pen.setWidth(1);
			painter.setPen(pen);
			while (edgNum>0)
			{
				txtInput>>xpoi>>ypoi;
				painter.drawLine(*(posx+xpoi)-minx,*(posy+xpoi)-miny,*(posx+ypoi)-minx,*(posy+ypoi)-miny);
				edgNum--;
			}
			f.close();
			pen.setColor(QColor(255,0,0));
			QBrush brush(QColor(255,0,0)); //画刷
			painter.setPen(pen);
			painter.setBrush(brush);
			for (int i=0;i<pNum;i++)
			{
				painter.drawEllipse(*(posx+i)-3-minx,*(posy+i)-3-miny,6,6);
			}
		}
		pen.setColor(QColor(colorr,0,colorb));
		pen.setWidth(2);
		painter.setPen(pen);
		for (int i=0;i<pathpoint.size()-1;i++)
		{
			painter.drawLine(*(posx+pathpoint[i])-minx,*(posy+pathpoint[i])-miny,*(posx+pathpoint[i+1])-minx,*(posy+pathpoint[i+1])-miny);
		}
		QBrush brush(QColor(255,255,0));
		painter.setBrush(brush);
		pen.setColor(QColor(0,0,0));
		pen.setWidth(1);
		painter.setPen(pen);
		for (int i=0;i<pathpoint.size();i++)
		{
			painter.drawEllipse(*(posx+pathpoint[i])-3-minx,*(posy+pathpoint[i])-3-miny,6,6);
		}
		//mark=0;
	}
}