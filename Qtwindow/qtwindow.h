#ifndef QTWINDOW_H
#define QTWINDOW_H

#include <QtGui/QMainWindow>  
#include <QString> 
#include <QtGui/QScrollArea>
#include <QResizeEvent>
#include "ui_qtwindow.h"

class Qtwindow : public QMainWindow,private Ui::QtwindowClass
{
	Q_OBJECT

public:
	Qtwindow(QWidget *parent = 0, Qt::WFlags flags = 0);
	~Qtwindow();
	void resizeEvent(QResizeEvent *event);
	int wlength;
	int wwidth;
private slots:
	void menuclick();
	void menuclick1();
	void drawgraph();
	void select(QString text);
private:
	Ui::QtwindowClass ui;
};

#endif // QTWINDOW_H
