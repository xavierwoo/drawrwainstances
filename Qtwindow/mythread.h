#ifndef MYTHREAD_H
#define MYTHREAD_H

#include <QThread>
#include "ui_qtwindow.h"
#include "mywidget.h"

class MyThread : public QThread,public Ui::QtwindowClass
{
	Q_OBJECT

public:
	MyThread();
	~MyThread();
protected:
     void run();
private:
	
};

#endif // MYTHREAD_H
