#ifndef MYWIDGET_H
#define MYWIDGET_H

#include <QWidget>
#include <QPainter>
#include <vector>
using namespace std;

class myWidget : public QWidget
{
	Q_OBJECT

public:
	vector< vector< vector< int > > > shortpath;
	myWidget(QWidget *parent);
	~myWidget();
	void paintEvent(QPaintEvent *);
public:
	QString fileName;          //导入无向图文件路径
	QString fileName1;         //导入工程文件路径
	int* posx;
	int* posy;
	int pNum,minx,miny,mark;             //证明绘图是否已经按下
	int colorr;
	int colorg;
	int colorb;
	vector<int> pathpoint;
private slots:

};

#endif // MYWIDGET_H
