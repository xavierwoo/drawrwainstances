/********************************************************************************
** Form generated from reading UI file 'qtwindow.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QTWINDOW_H
#define UI_QTWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QListWidget>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QStatusBar>
#include <QtGui/QWidget>
#include <mywidget.h>

QT_BEGIN_NAMESPACE

class Ui_QtwindowClass
{
public:
    QAction *readaction;
    QAction *readaction1;
    QAction *drawButton;
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout;
    QListWidget *mylistWidget;
    myWidget *drawWidget;
    QMenuBar *menuBar;
    QMenu *menuXitong;
    QMenu *menu;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *QtwindowClass)
    {
        if (QtwindowClass->objectName().isEmpty())
            QtwindowClass->setObjectName(QString::fromUtf8("QtwindowClass"));
        QtwindowClass->resize(776, 617);
        QtwindowClass->setMinimumSize(QSize(0, 0));
        QtwindowClass->setMaximumSize(QSize(16777215, 16777215));
        readaction = new QAction(QtwindowClass);
        readaction->setObjectName(QString::fromUtf8("readaction"));
        readaction1 = new QAction(QtwindowClass);
        readaction1->setObjectName(QString::fromUtf8("readaction1"));
        drawButton = new QAction(QtwindowClass);
        drawButton->setObjectName(QString::fromUtf8("drawButton"));
        centralWidget = new QWidget(QtwindowClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        horizontalLayout = new QHBoxLayout(centralWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        mylistWidget = new QListWidget(centralWidget);
        mylistWidget->setObjectName(QString::fromUtf8("mylistWidget"));
        mylistWidget->setMinimumSize(QSize(150, 0));

        horizontalLayout->addWidget(mylistWidget);

        drawWidget = new myWidget(centralWidget);
        drawWidget->setObjectName(QString::fromUtf8("drawWidget"));
        drawWidget->setMinimumSize(QSize(600, 500));

        horizontalLayout->addWidget(drawWidget);

        QtwindowClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(QtwindowClass);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 776, 21));
        menuXitong = new QMenu(menuBar);
        menuXitong->setObjectName(QString::fromUtf8("menuXitong"));
        menu = new QMenu(menuBar);
        menu->setObjectName(QString::fromUtf8("menu"));
        QtwindowClass->setMenuBar(menuBar);
        statusBar = new QStatusBar(QtwindowClass);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        QtwindowClass->setStatusBar(statusBar);

        menuBar->addAction(menuXitong->menuAction());
        menuBar->addAction(menu->menuAction());
        menuXitong->addAction(readaction);
        menuXitong->addAction(readaction1);
        menu->addAction(drawButton);

        retranslateUi(QtwindowClass);

        QMetaObject::connectSlotsByName(QtwindowClass);
    } // setupUi

    void retranslateUi(QMainWindow *QtwindowClass)
    {
        QtwindowClass->setWindowTitle(QApplication::translate("QtwindowClass", "Qtwindow", 0, QApplication::UnicodeUTF8));
        readaction->setText(QApplication::translate("QtwindowClass", "\345\257\274\345\205\245\346\227\240\345\220\221\345\233\276\346\226\207\344\273\266 ", 0, QApplication::UnicodeUTF8));
        readaction1->setText(QApplication::translate("QtwindowClass", "\345\257\274\345\205\245\345\267\245\347\250\213\346\226\207\344\273\266", 0, QApplication::UnicodeUTF8));
        drawButton->setText(QApplication::translate("QtwindowClass", "\347\273\230\345\233\276", 0, QApplication::UnicodeUTF8));
        menuXitong->setTitle(QApplication::translate("QtwindowClass", "\347\263\273\347\273\237", 0, QApplication::UnicodeUTF8));
        menu->setTitle(QApplication::translate("QtwindowClass", "\345\212\237\350\203\275", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class QtwindowClass: public Ui_QtwindowClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QTWINDOW_H
