/****************************************************************************
** Meta object code from reading C++ file 'qtwindow.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../qtwindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qtwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Qtwindow[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      10,    9,    9,    9, 0x08,
      22,    9,    9,    9, 0x08,
      35,    9,    9,    9, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_Qtwindow[] = {
    "Qtwindow\0\0menuclick()\0menuclick1()\0"
    "drawgraph()\0"
};

void Qtwindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Qtwindow *_t = static_cast<Qtwindow *>(_o);
        switch (_id) {
        case 0: _t->menuclick(); break;
        case 1: _t->menuclick1(); break;
        case 2: _t->drawgraph(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData Qtwindow::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Qtwindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_Qtwindow,
      qt_meta_data_Qtwindow, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Qtwindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Qtwindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Qtwindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Qtwindow))
        return static_cast<void*>(const_cast< Qtwindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int Qtwindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
