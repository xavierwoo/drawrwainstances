#include "qtwindow.h"
#include <QFileDialog>
#include <qtextstream.h>
#include <QString>
#include <QMessageBox>
#include <QListWidget>
#include<vector>
#include<stack>
#include<cmath>
#include <time.h>
#include <QProgressDialog>
using namespace std;
#define L 400     //显示窗口边长
#define K 0.1   //弹力常数
#define PI  3.14159265358979323846
#define mymark 0.5               //精确度

void initialize(int*& d,double*& k1,double*& l,QTextStream& txtInput,int pNum,int edgNum,vector<vector<vector<int> > >& shortpath);           //初始化函数
void buildPos(int*& x,int*& y,int pNum);                                               //初始化点的位置
double getDelta(int m,double* k,double* l,int pNum,int* x,int* y);                      //获取delta
double getMaxDelta(double* k,double* l,int pNum,int* x,int* y,int &changepos);          //获取最大的delta
void getTheta(int m,double* l,double* k,int* x,int* y,int pNum,double& thetax,double& thetay);       //获得thetax和thetay
void getmin(int& x,int& y,int pNum,int& minx,int& miny);

void initialize(int*& d,double*& k1,double*& l,QTextStream& txtInput,int pNum,int edgNum,vector<vector<vector<int> > >& shortpath)
{
	int m,n,maxd=0,length=pNum*pNum;
	shortpath.clear();
	vector<int> temp1;
	vector< vector<int> > temp2;
	for (int j=0;j<pNum;j++)
	{
		temp2.push_back(temp1);
	}
	for (int k=0;k<pNum;k++)
	{
		shortpath.push_back(temp2);
	}
	for (int j=0;j<pNum;j++)
	{
		for(int k=0;k<pNum;k++)
		{
			if (k==j)
			{
				*(d+j*pNum+k)=0;
			}
			else
			{
				*(d+j*pNum+k)=pNum;
			}
		}
	}
	while (--edgNum>=0)
	{
		txtInput>>m>>n;
		*(d+m*pNum+n)=1;
	}
	for (int i=0;i<pNum;i++)
	{
		for (m=0;m<pNum;m++)
		{
			for (n=0;n<pNum;n++)
			{
				if (*(d+m*pNum+n)>*(d+m*pNum+i)+*(d+i*pNum+n))
				{
					*(d+m*pNum+n)=*(d+m*pNum+i)+*(d+i*pNum+n);
					shortpath[m][n].push_back(i);
				}
			}
		}
	}
	for (int i=0;i<length;i++)                        //获得做大距离maxd
	{
		if (maxd<*(d+i))
		{
			maxd=*(d+i);
		}
	}
	for (int j=0;j<pNum;j++)                          //计算l
	{
		for(int k=0;k<pNum;k++)
		{
			*(l+j*pNum+k)=(double)L*(*(d+j*pNum+k))/maxd;
		}
	}
	for (int j=0;j<pNum;j++)                          //计算k
	{
		for(int k=0;k<pNum;k++)
		{
			if (k==j)
			{
				*(k1+j*pNum+k)=0;
			}
			else
			{
				*(k1+j*pNum+k)=(double)K/(*(d+j*pNum+k)**(d+j*pNum+k));
			}
		}
	}
}

void buildPos(int*& x,int*& y,int pNum)
{
	double p=0,t=2*PI/pNum;
	for (int i=0;i<pNum;i++)
	{

		*(x+i)=L/2.0*(cos(p)+1);
		*(y+i)=L/2.0*(sin(p)+1);
		p+=t;
	}
}

double getDelta(int m,double* k,double* l,int pNum,int* x,int* y)
{
	double delx=0,dely=0;
	for (int i=0;i<pNum;i++)
	{
		if (i!=m)
		{
			double cx=*(x+m)-*(x+i);
			double cy=*(y+m)-*(y+i);
			delx+=*(k+m*pNum+i)*(cx-(double)*(l+m*pNum+i)*cx/pow(pow(cx,2.0)+pow(cy,2.0),0.5));
			dely+=*(k+m*pNum+i)*(cy-(double)*(l+m*pNum+i)*cy/pow(pow(cx,2.0)+pow(cy,2.0),0.5));
		}
	}
	return pow(pow(delx,2.0)+pow(dely,2.0),0.5);
}

double getMaxDelta(double* k,double* l,int pNum,int* x,int* y,int &changepos)
{
	double deltatemp,delta=0;
	for (int i=0;i<pNum;i++)
	{
		deltatemp=getDelta(i,k,l,pNum,x,y);
		if (delta<deltatemp)
		{
			delta=deltatemp;
			changepos=i;
		}
	}
	return delta;
}

void getTheta(int m,double* l,double* k,int* x,int* y,int pNum,double& thetax,double& thetay)
{
	double A=0,B=0,C=0,D=0,E=0; 
	for (int i=0;i<pNum;i++)
	{
		if (i!=m)
		{
			double cx=*(x+m)-*(x+i);
			double cy=*(y+m)-*(y+i);
			double xydevide=pow(pow(cx,2.0)+pow(cy,2.0),1.5);
			A+=*(k+m*pNum+i)*(1-((double)*(l+m*pNum+i)*pow(cy,2.0)/xydevide));
			B+=(double)*(k+m*pNum+i)*(*(l+m*pNum+i)*cx*cy/xydevide);
			C+=*(k+m*pNum+i)*(1-((double)*(l+m*pNum+i)*pow(cx,2.0)/xydevide));
			D+=*(k+m*pNum+i)*(cx-(double)*(l+m*pNum+i)*cx/pow(pow(cx,2.0)+pow(cy,2.0),0.5));
			E+=*(k+m*pNum+i)*(cy-(double)*(l+m*pNum+i)*cy/pow(pow(cx,2.0)+pow(cy,2.0),0.5));
		}
	}
	thetax=(double)(C*D-B*E)/(B*B-A*C);
	thetay=(double)(A*E-B*D)/(B*B-A*C);
}

void getmin(int* x,int* y,int pNum,int& minx,int& miny)
{
	for (int i=0;i<pNum;i++)
	{
		if (minx>*(x+i))
		{
			minx=*(x+i);
		}
		if (miny>*(y+i))
		{
			miny=*(y+i);
		}
	}
}

void drawandculculate(QFile& f,myWidget*& child,QProgressDialog& process)                    //核心算法函数
{
	srand((unsigned) time(NULL));
	QTextStream txtInput(&f);
	vector<vector<int> > set;
	int pNum,edgNum,changepos,xpos,ypos,mindeletanum=0,changepostemp=-1,changenum=0;
	int* d;
	double * k,* l,delta,thetax,thetay,mindelta;                          //d:点的距离表、k表、l表、X轴位置、Y轴位置
	txtInput>>pNum>>edgNum;
	child->pNum=pNum;
	d=new int[pNum*pNum];
	k=new double[pNum*pNum];
	l=new double[pNum*pNum];
	child->posx=new int[pNum];
	child->posy=new int[pNum];
	initialize(d,k,l,txtInput,pNum,edgNum,child->shortpath);
	f.close();
	buildPos(child->posx,child->posy,pNum);
	for(int i=0;i<15000;i++)
		//while(true)
	{
		delta=getMaxDelta(k,l,pNum,child->posx,child->posy,changepos);
		if (delta<mymark)
		{
			break;
		}
		mindelta=1000000;
		if (changepostemp==changepos)
		{
			changenum++;
		}
		if (changenum>0)
		{
			changepos=rand()%pNum;
			changenum=0;
		}
		int itrel=0;
		while ((delta=getDelta(changepos,k,l,pNum,child->posx,child->posy))>mymark)
		{
			getTheta(changepos,l,k,child->posx,child->posy,pNum,thetax,thetay);
			*(child->posx+changepos)+=thetax;
			*(child->posy+changepos)+=thetay;
			if (mindelta>delta)
			{
				mindelta=delta;
				mindeletanum=0;
			}
			else if (mindelta==delta)
			{
				mindeletanum++;
			}
			if (mindeletanum>0)
			{
				mindeletanum=0;
				break;
			}
			if (itrel==200)
			{
				mindelta=delta;
				itrel=0;
				mindeletanum=0;
			}
			itrel++;
		}
		changepostemp=changepos;
		process.setValue(i);
	}
	getmin(child->posx,child->posy,pNum,child->minx,child->miny);
	child->minx-=10;
	child->miny-=10;
}

Qtwindow::Qtwindow(QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags)
{
	setupUi(this);
	wlength=this->rect().height();
	wwidth=this->width();
	connect(readaction,SIGNAL(triggered()),this,SLOT(menuclick()));
	connect(readaction1,SIGNAL(triggered()),this,SLOT(menuclick1()));
	connect(drawButton,SIGNAL(triggered()),this,SLOT(drawgraph()));
	connect(mylistWidget,SIGNAL(currentTextChanged(QString)),this,SLOT(select(QString)));
}

Qtwindow::~Qtwindow()
{

}

void showlist(QListWidget* mylistview,QFile& f)               //list控件显示函数
{
	mylistview->clear();
	QTextStream txtInput(&f);  
	QString lineStr;  
	lineStr = txtInput.readLine();
	lineStr = txtInput.readLine();
	lineStr = "业务";
	mylistview->addItem(lineStr);
	while(!txtInput.atEnd())  
	{  
		lineStr = txtInput.readLine();  
		mylistview->addItem(lineStr);
	}  
	f.close();
}

void Qtwindow::menuclick()                //菜单的导入按钮
{
	drawWidget->mark=0;
	drawWidget->fileName=QFileDialog::getOpenFileName(this,"导入文件","./","ALL FILE(*.*)");
	if (drawWidget->fileName!=NULL)
	{
		QMessageBox::information(this,"提示","导入文件成功");
		mylistWidget->clear();
		drawWidget->fileName1.clear();
		drawWidget->shortpath.clear();
	}
	else
	{
		QMessageBox::information(this,"提示","导入文件失败");
		drawWidget->shortpath.clear();
		drawWidget->fileName.clear();
		drawWidget->fileName1.clear();
		mylistWidget->clear();
	}
}

void Qtwindow::menuclick1()
{
	if (drawWidget->shortpath.size()==0)
	{
		QMessageBox::information(this,"警告","请先导入无向图文件并生成图");
	}
	else
	{
		drawWidget->mark=0;
		drawWidget->fileName1=QFileDialog::getOpenFileName(this,"导入文件","./","ALL FILE(*.*)");
		if (drawWidget->fileName1!=NULL)
		{
			QMessageBox::information(this,"提示","导入文件成功");
			QFile f(drawWidget->fileName1);  
			if(!f.open(QIODevice::ReadOnly | QIODevice::Text))  
			{  
				QMessageBox::information(this,"警告","打开文件失败"); 
			}  
			else
			{
				showlist(mylistWidget,f);
			}
		}
		else
		{
			drawWidget->mark=0;
			QMessageBox::information(this,"提示","导入文件失败");
		    drawWidget->fileName1.clear();
			mylistWidget->clear();
			drawWidget->mark=1;
			drawWidget->repaint();
		}
		if (drawWidget->fileName!=NULL&&drawWidget->fileName1!=NULL)
		{
			drawWidget->mark=1;
			drawWidget->repaint();
		}
	}
}

void Qtwindow::drawgraph()                        //无向图坐标计算函数
{
	drawWidget->mark=0;
	if(drawWidget->fileName!=NULL)
	{
		QFile f(drawWidget->fileName);
		if(!f.open(QIODevice::ReadOnly | QIODevice::Text))  
		{  
			QMessageBox::information(this,"警告","打开文件失败"); 
		}  
		else
		{
			mylistWidget->clear();
			QProgressDialog process(this);
			process.setLabelText(tr("processing..."));
			process.setRange(0,15000);
			process.setModal(true);
			process.setCancelButtonText(0);
			drawandculculate(f,drawWidget,process);
			drawWidget->mark=1;
			drawWidget->repaint();
		}
	}
	else
	{
		QMessageBox::information(this,"警告","请先导入无向图文件");
	}
}

void Qtwindow::resizeEvent(QResizeEvent *event)            //重写的改变窗口尺寸的响应函数，使绘制图像重绘
{
	int w=this->width();
	int l=this->rect().height();
	if ((w!=wwidth||l!=wlength)&&drawWidget->fileName!=NULL)
	{
		//drawWidget->mark=1;
		drawWidget->repaint();
		wlength=l;
		wwidth=w;
	}
}

void Qtwindow::select(QString text)               //点选list控件函数
{
	if (text.size()>2)
	{
		drawWidget->pathpoint.clear();
		//vector<int> pathpoint,pathpointtemp;
		//stack<int> s;
		QStringList sl=text.split(" ");
		drawWidget->colorr=sl.at(1).toInt();
		if (drawWidget->colorr%2==1)
		{
			drawWidget->colorg=drawWidget->colorr/2;
			drawWidget->colorb=drawWidget->colorr*4;
		}
		else
		{
			drawWidget->colorg=drawWidget->colorr/2;
			drawWidget->colorb=drawWidget->colorr;
			drawWidget->colorr=drawWidget->colorr*4;
		}
		int num=sl.at(2).toInt();
		//int a,b;
		for(int i=0;i<num;i++)
		{
			drawWidget->pathpoint.push_back(sl.at(3+i).toInt());
		}
		/**for (int i=0;i<pathpointtemp.size();i++)
		{
			if(i==0)
			{
				pathpoint.push_back(pathpointtemp[i]);
			}
			else
			{
				a=pathpoint.back();
				b=pathpointtemp[i];
				s.push(b);
				while (!s.empty())
				{
					if (drawWidget->shortpath[a][b].size()!=0)
					{
						for (int k=drawWidget->shortpath[a][b].size()-1;k>=0;k--)
						{
							s.push(drawWidget->shortpath[a][b][k]);
						}
					}
					else
					{
						a=s.top();
						s.pop();
						pathpoint.push_back(a);
					}
					if (!s.empty())
					{
						b=s.top();
					}
				}
			}
		}
		for(int i=0;i<pathpoint.size();i++)
		{
			drawWidget->pathpoint.push_back(pathpoint[i]);
		}**/
		drawWidget->mark=2;
		drawWidget->repaint();
	}
}